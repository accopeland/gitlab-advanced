# Advanced gitlab tutorial #

This repository contains material for the advanced gitlab tutorial at the JGI, June 2017.

## Pre-requisites ##

You are expected to be familiar with **git**, **docker** and **gitlab**. To that end, please make sure you've studied the earlier tutorials:
- Basic use of git and docker: **https://www.nersc.gov/assets/Uploads/Git+Docker-Tutorial-Dec01-2016.pdf**
  - in particular, make sure you understand exercises 4 and 5 in the **git** directory of the accompanying repository.
- Introduction to gitlab: **https://www.nersc.gov/assets/Uploads/2017-02-06-Gitlab-CI.pdf**
  - especially make sure you can do the first exercise.

You also need an account on **gitlab.com**, and to have up to date versions of **git** (version **2.11** or higher) and **docker** (version **17.03.1-ce** or higher) installed on your laptop.

## The exercises ##
Read the PowerPoint slides or the PDF for guidelines, then work through the exercises one by one. Check the **md** files for details about each exercise.
